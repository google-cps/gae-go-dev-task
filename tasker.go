package tasker

import (
	"fmt"
	"net/http"
	"google.golang.org/appengine"
	"google.golang.org/appengine/taskqueue"
)

func Sender(w http.ResponseWriter, r *http.Request) {
	name = "invalid/name"
	
	w.Header().Set("Content-Type", "text/plain")
	w.WriteHeader(200)
	
	task := taskqueue.NewPOSTTask("/receive", map[string][]string{"name": {name}})

	ctx := appengine.NewContext(r)
	_, err := taskqueue.Add(ctx, task, "default")
	if err != nil {
		fmt.Fprintf(w, "Could not add task %s\n%+V\n", name, err)
		return
	}

	fmt.Fprintf(w, "Added task %s\n", name)
	return
}

func init() {
	http.HandleFunc("/send", Sender)
}
