According to the [Naming a task][0] documentation for the App Engine standard environment, a task name can contain uppercase and lowercase letters, numbers, underscores, and hyphens.  The maximum length for a task name is 500 characters.  This is not currently in effect for the Go runtime running locally with the Development server.  It is possible to add a task whose name contains a `/` to a task queue when using dev_appserver.py in Go.  This is the case as of this writing (2017-02-01).

This can be reproduced with the contents of this reproduction:

```shell
go get "google.golang.org/appengine"
go get "google.golang.org/appengine/taskqueue"
dev_appserver.py app.yaml
curl localhost:8080/send
```

[0]: https://cloud.google.com/appengine/docs/go/taskqueue/push/creating-tasks#naming_a_task